from django.urls import path

from . import views


urlpatterns = [
    path('home', views.realhome, name='home'),
    path('accueil', views.realhome, name='home'),
    path('', views.home),
    path('contact', views.contact),
    path('about', views.about)
]