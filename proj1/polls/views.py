from django.shortcuts import render
from django.http import HttpResponse


def home(request):
    return render(request, 'polls/home.html')


def realhome(request):
    return HttpResponse('<h2> hello this is secret home </h2>')


def contact(request) :
    return render(request, 'polls/contact.html')


def about(request) :
    return render(request, 'polls/about.html')
# Create your views here.
